package io.rainrobot.wake.rest.configuration.appuser.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.rainrobot.wake.rest.configuration.appuser.AppUser;

@Repository("userDao")
public interface AppUserRepo extends CrudRepository<AppUser, Integer> {
	
	public AppUser findByUsername(String username);

	public boolean existsByUsername(String username);

}
