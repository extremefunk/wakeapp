package io.rainrobot.wake.rest;

import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

@Configuration
@TestPropertySource("test.properties")
public class TestConfig {
}
