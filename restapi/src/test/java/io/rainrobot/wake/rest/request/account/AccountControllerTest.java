package io.rainrobot.wake.rest.request.account;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import io.rainrobot.wake.core.Path;
import io.rainrobot.wake.rest.configuration.appuser.AppAuth;
import io.rainrobot.wake.rest.configuration.appuser.AppUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Service.class)
//@ContextConfiguration(classes = {MvcConfig.class, SecurityConfiguration.class})
@WebMvcTest(AccountController.class)
public class AccountControllerTest {
    @Autowired
    private MockMvc mvc;
//    @MockBean
//    private UserService usrService;
    @MockBean
    private AccountService service;
//    @Autowired
//    private WebApplicationContext wac;
//
//    @Before
//    public void setUp() throws Exception {
//        mvc = MockMvcBuilders
//                .webAppContextSetup(wac)
//                .apply(SecurityMockMvcConfigurers.springSecurity())
//                .build();
//    }

    @Test
    public void getAccount() {
    }

    @Test
    public void updateAccountName() {
    }

    @Test
    public void deleteAccount() {
    }

    @Test
    public void getAccountName() throws Exception {

        String usrNm = "admin";
        AppUser appUser = new AppUser(usrNm, "pass");
//        HttpHeaders headers = new HttpHeaders();
//        Mockito.when(usrService.findByUsername(usrNm)).thenReturn(appUser);
//        String token = Values.TOKEN_PREFIX + TokenBuilder.create(usrNm);
//        headers.set(Values.AUTH_HEADER, token);

        mvc.perform(get(Path.USERNAME)
                .with(authentication(new AppAuth(appUser, true))))
//                .header(Values.AUTH_HEADER, token))
                .andExpect(content().string(usrNm));
    }

    @Test
    public void getLastChange() {
    }
}