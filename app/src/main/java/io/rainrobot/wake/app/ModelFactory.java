package io.rainrobot.wake.app;

import io.rainrobot.wake.client.AccountClient;
import io.rainrobot.wake.client.AlarmEventClient;
import io.rainrobot.wake.client.ClientFactory;
import io.rainrobot.wake.client.DeviceClient;
import io.rainrobot.wake.client.PresetClient;
import io.rainrobot.wake.client.SignupClient;
import io.rainrobot.wake.model.DeviceEventsModel;
import io.rainrobot.wake.model.DeviceRegModel;
import io.rainrobot.wake.model.EventListModel;
import io.rainrobot.wake.model.IsNewDeviceModel;
import io.rainrobot.wake.model.LoginModel;
import io.rainrobot.wake.model.MainMenuModel;
import io.rainrobot.wake.model.PresetsModel;
import io.rainrobot.wake.model.SelectDeviceModel;
import io.rainrobot.wake.model.SettingsModel;
import io.rainrobot.wake.model.SighupModel;

public class ModelFactory {

    private ClientFactory clientFactory;
    private DeviceInfoMgr deviceInfoMgr;
    private LoginMgr loginMgr;
    private EventUpdateMgr eventUpdateMgr;

    public ModelFactory(ClientFactory clientFactory, DeviceInfoMgr deviceInfoMgr, LoginMgr loginMgr, EventUpdateMgr eventUpdateMgr) {
        this.clientFactory = clientFactory;
        this.deviceInfoMgr = deviceInfoMgr;
        this.loginMgr = loginMgr;
        this.eventUpdateMgr = eventUpdateMgr;
    }

    public LoginModel getLogin() {
        return new LoginModel(loginMgr);
    }

    public MainMenuModel getMainMenu() {
        return new MainMenuModel(loginMgr);
    }

    public SighupModel getSignup() {
        SignupClient client = clientFactory.getSignupClient();
        return new SighupModel(client, loginMgr, deviceInfoMgr);
    }

    public PresetsModel getPresets() {
        PresetClient client = clientFactory.getPresetClient();
        return new PresetsModel(client, eventUpdateMgr);
    }

    public SettingsModel getSettings() {
        DeviceClient client = clientFactory.getDeviceClient();
        AccountClient accountClient = clientFactory.getAccountClient();
        return new SettingsModel(accountClient, client, deviceInfoMgr);
    }

    public DeviceEventsModel getDeviceEvents() {
        return new DeviceEventsModel(deviceInfoMgr);
    }

    public EventListModel getEventList() {
        DeviceClient deviceClient = clientFactory.getDeviceClient();
        PresetClient presetClient = clientFactory.getPresetClient();
        AlarmEventClient eventClient = clientFactory.getEventClient();
        return new EventListModel(eventClient, presetClient, deviceClient, eventUpdateMgr);
    }

    public DeviceRegModel getDeviceReg() {
        DeviceClient client = clientFactory.getDeviceClient();
        AccountClient accountClient = clientFactory.getAccountClient();
        return new DeviceRegModel(client, accountClient, deviceInfoMgr);
    }

    public SelectDeviceModel getSelectDevice() {
        DeviceClient client = clientFactory.getDeviceClient();
        AccountClient accountClient = clientFactory.getAccountClient();
        return new SelectDeviceModel(deviceInfoMgr, client, accountClient);
    }

    public IsNewDeviceModel getIsNewDevice() {
        return new IsNewDeviceModel();
    }
}
