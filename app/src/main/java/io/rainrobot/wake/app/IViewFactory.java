package io.rainrobot.wake.app;

import io.rainrobot.wake.view.DeviceEventsView;
import io.rainrobot.wake.view.DeviceRegView;
import io.rainrobot.wake.view.EventListView;
import io.rainrobot.wake.view.IsNewDeviceView;
import io.rainrobot.wake.view.LoginView;
import io.rainrobot.wake.view.MainMenuView;
import io.rainrobot.wake.view.PresetsView;
import io.rainrobot.wake.view.SelectDeviceView;
import io.rainrobot.wake.view.SettingsView;
import io.rainrobot.wake.view.SingupView;
import io.rainrobot.wake.view.TextBoxView;

public interface IViewFactory {

    LoginView getLoginView();

    TextBoxView getTextBoxView();

    SingupView getSingupView();

    SettingsView getSettingsView();

    SelectDeviceView getSelectDeviceView();

    PresetsView getPresetsView();

    MainMenuView getMainMenuView();

    EventListView getEventListView();

    DeviceRegView getDeviceRegView();

    DeviceEventsView getDeviceEventsView();

    IsNewDeviceView getIsNewDeviceView();
}
