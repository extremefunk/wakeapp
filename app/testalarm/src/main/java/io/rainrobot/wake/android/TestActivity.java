package io.rainrobot.wake.android;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;


import java.util.Calendar;
import java.util.Date;

import io.rainrobot.wake.alarm.IAlarmMgr;
import io.rainrobot.wake.alarm.ISchedulerServiceMgr;
import io.rainrobot.wake.android.activities.R;
import io.rainrobot.wake.android.configuration.AndroidAppConfiguration;
import io.rainrobot.wake.android.configuration.WakeActivity;
import io.rainrobot.wake.core.AlarmEvent;
import io.rainrobot.wake.core.Preset;
import io.rainrobot.wake.core.Sound;

public class TestActivity extends WakeActivity {

    private Integer id = 1;
    private AndroidAppConfiguration config;
    private IAlarmMgr alarmMgr;
    private ISchedulerServiceMgr scheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        config = new AndroidAppConfiguration(getWakeApplication());
        scheduler = config.getAlarmSchedulerMgr();
        alarmMgr = config.getAlarmMgr();
        setSchedulerBtn();
        setSetBtn();
        setCancelBtn();
    }

    private void setSchedulerBtn() {
        Button onBtn = findViewById(R.id.schedulerOnBtn);
        onBtn.setOnClickListener((v) -> {
            scheduler.turnOn();
        });
        Button offBtn = findViewById(R.id.schedulerOffBtn);
        offBtn.setOnClickListener((v) -> {
            scheduler.turnOff();
        });
    }

    private void setCancelBtn() {
        Button btn = findViewById(R.id.cancelBtn);
        btn.setOnClickListener((v) -> {
            AlarmEvent event = new AlarmEvent();
            event.setId(id);
                alarmMgr.removeAlarm(event);
        });
    }

    private void setSetBtn() {
        Button btn = findViewById(R.id.setAlarmBtn);
        btn.setOnClickListener((v) -> {
            AlarmEvent event = getEvent();
                alarmMgr.addAlarm(event, event.getTime());
        });
    }

    private AlarmEvent getEvent() {
        int shutoff = Integer
                .parseInt(
                        ((EditText)findViewById(R.id.turnOffIn))
                                .getText()
                                .toString()
                );
        int snooze = Integer
                .parseInt(
                        ((EditText)findViewById(R.id.snoozeIn))
                                .getText()
                                .toString()
                );
        return new AlarmEvent.Builder()
                .setId(id)
                .setDelay(0)
                .setPreset(getPreset())
                .setSound(Sound.RING)
                .setVol(10)
                .setShutOff(shutoff)
                .setSnooze(snooze)
                .build();
    }

    private Preset getPreset() {
        Preset preset = new Preset();
        preset.setId(1);
        preset.setTime(getTime());
        return preset;
    }

    private Date getTime() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, 5
           );
        return c.getTime();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test;
    }
}
