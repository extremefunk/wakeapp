package io.rainrobot.wake.client;

import io.rainrobot.wake.client.requestsender.IHttpRequestSender;
import io.rainrobot.wake.core.AlarmEvent;

public class AlarmEventClient {
	
	private IHttpRequestSender sender;
	private final String URL;

	public AlarmEventClient(IHttpRequestSender sender, String url){
		this.sender = sender;
		this.URL = url;
	}

	public AlarmEvent createAlarmEvent(Integer presetParentId) {
		return sender.sendWithParam(URL, presetParentId.toString(),
				HttpMethodEnum.POST, AlarmEvent.class, presetParentId);
	}
	
	public void updateAlarmEvent(AlarmEvent alarmevent) {
		sender.send(URL, HttpMethodEnum.PUT, Void.class, alarmevent);
	}
	
	public void deleteAlarmEvent(AlarmEvent event) {
		Integer id = event.getId();
		sender.sendWithParam(URL, id.toString(), HttpMethodEnum.DELETE, Void.class, null);
	}

	public AlarmEvent[] getAllEvents(Integer preset_id) {
		return sender.getWithParam(URL, preset_id.toString(), AlarmEvent[].class);
	}

}
