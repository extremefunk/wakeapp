package io.rainrobot.wake.client;

import io.rainrobot.wake.client.requestsender.IHttpRequestSender;

public class TestTokenClient {

	private IHttpRequestSender sender;
	private final String url;

	public TestTokenClient(IHttpRequestSender sender, String url) {
		this.sender = sender;
		this.url = url;
	}

	public Boolean testToken() {
		return sender.get(url, Boolean.class);
	}
}
