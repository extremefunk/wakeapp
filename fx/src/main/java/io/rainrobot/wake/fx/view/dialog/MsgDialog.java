package io.rainrobot.wake.fx.view.dialog;

import io.rainrobot.wake.fx.Res;
import io.rainrobot.wake.fx.view.component.MainStageMgr;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

public class MsgDialog extends OkDialog {
    private Label label;

    public MsgDialog(MainStageMgr mainStageMgr) {
        super(mainStageMgr);
        scene.setFill(Color.TRANSPARENT);
        root.getStylesheets().add(Res.getCss("MsgDialog"));
    }

    public void show(String msg) {
        label.setText(msg);
        super.show();
    }

    @Override
    protected Parent getContent() {
        label = new Label();
        label.setAlignment(Pos.CENTER);
        label.setPadding(new Insets(10));
        return label;
    }
}
